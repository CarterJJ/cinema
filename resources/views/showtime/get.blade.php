@extends('layouts.app')

@section('content')
<div class="panel-heading">
  Показать расписание
</div>
<div class="panel-body">
  <div class="container col-md-10 col-md-offset-1">
    <form class="form-horizontal" action="" method="GET">
      <div class="form-group">
        <label for="dateList">Дата:</label>
        <select class="form-control input-md" name="date" id="dateList">
          @foreach($dates as $date)
            @if(isset($_GET['date']) and $date == $_GET['date'])
              <option value="{{ $date }}" selected="selected">{{ $date }}</option>
            @else
              <option value="{{ $date }}">{{ $date }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="sortBySubj">Сортировать по:</label>
        <select class="form-control input-md" name="subj" id="sortBySubj">
          @foreach($subj_select as $key => $value)
            @if(isset($_GET['subj']) and $key == $_GET['subj'])
              <option value="{{ $key }}" selected="selected">{{ $value }}</option>
            @else
              <option value="{{ $key }}">{{ $value }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="sortByDir">И по:</label>
        <select class="form-control input-md" name="dir" id="sortByDir">
          @foreach($dir_select as $key => $value)
            @if(isset($_GET['dir']) and $key == $_GET['dir'])
              <option value="{{ $key }}" selected="selected">{{ $value }}</option>
            @else
              <option value="{{ $key }}">{{ $value }}</option>
            @endif
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-default col-md-6 col-md-push-3">Показать</button>
      </div>
    </form>
  </div>
</div>
@stop

@section('table')
@if(isset($table))
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-push-1">
      <div class="panel panel-default">
        <div class="panel-heading">
          Расписание сеансов в кинотеатре Салават
        </div>
        <div class="panel-body">
          <table class="table table-striped table-hover">
            <tr>
              <th>Время</th><th>Название</th><th>Цена</th><th>Рейтинг</th>
            </tr>
            @foreach($table as $row)
              <tr>
                <td>{{ substr(($row->time), 0, 5) }}</td><td>{{ $row->title }}</td><td>{{ $row->price }}</td><td>{{ $row->rating.'+' }}</td>
              </tr>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@stop