<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cinema</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <style>
      body {
        padding-top: 70px;
      }
      .btn {
        font-size: 20px;
      }
      .fa-btn {
        margin-right: 6px;
      }
      .nav>li>a {
        font-size: 15px;
      }
      .navbar {
        background-color: #cdd;
      }
      .navbar-header {
        background-color: #bcc;
      }
      .navbar-header>a {
        color: #888;
      }
      .navbar-header>a:hover {
        color: #587;
      }
      .nav.navbar-nav>li>a {
        color: #666;
        background-color: #cdd;
      }
      .nav.navbar-nav>li>a:hover {
        background-color: #bcc;
      }
      .nav.navbar-nav>li>a:active {
        background-color: #bcc;
      }
      .panel-body>p {
        font-weight: lighter;
        font-size: 18px;
        color: #8aa;
      }
    </style>
  </head>
  <body id="app-layout">
    <nav class="navbar navbar-fixed-top">
      <div class="container nb">
        <div class="navbar-header col-md-1">
          {!! link_to_route('mainPage', 'Cinema', [], ['class' => 'navbar-brand']) !!}
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li>{!! link_to_route('mainPage', 'Главная') !!}</li>
            <li>{!! link_to_route('update', 'Обновить расписание') !!}</li>
            <li>{!! link_to_route('getForm', 'Получить расписание') !!}</li>
          </ul>
        </div>
      </div>
    </nav>

    @if (session('updated'))
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-push-1">
          <div class="panel panel-success">
            <div class="panel-heading">
              Обновление успешно
            </div>
            <div class="panel-body">
              Расписание было успешно обновлено
            </div>
          </div>
        </div>
      </div>
    </div>
    @endif

    <div class="container">
      <div class="row">
        <div class="col-md-{{ $size }} col-md-offset-{{ $offset }}">
          <div class="panel {{ isset($panel) ? $panel : 'panel-default' }}">
            @yield('content')
          </div>
        </div>
      </div>
    </div>

  @yield('table')


  <!-- JavaScripts -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
  </body>
</html>
