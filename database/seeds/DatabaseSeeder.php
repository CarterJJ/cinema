<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Page;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    	$this->call(PageSeeder::class);
    }
}

class PageSeeder extends Seeder {
	public function run()
	{
		DB::table('pages')->delete();
		Page::create([
				'date' => '2016-8-18',
				'time' => time(),
				'name' => 'test_name',
				'price' => 100,
				'rating' => 16
			]);
	}
}
