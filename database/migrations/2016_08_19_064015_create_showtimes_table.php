<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowtimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('showtimes', function (Blueprint $table) {
        $table->increments('id');
        $table->date('date');
        $table->date('second_date')->nullable()->default(NULL);
        $table->time('time')->unsinged();
        $table->string('title');
        $table->integer('price')->unsinged();
        $table->smallInteger('rating')->unsinged();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('showtimes');
    }
  }
