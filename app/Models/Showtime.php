<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Goutte\Client;

class Showtime extends Model
{
	private function getDateFormatted($pre_date) {
		$result_date = [];
		$day = $pre_date[0];
		$second_day = count($pre_date) === 3 ? $pre_date[1] : NULL;
		$month = str_replace(['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'], ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'], $pre_date[count($pre_date)-1]);
		$year = date('Y', time());

		$result_date['date'] = $year.'-'.$month.'-'.$day;
		if ($second_day) {
			$result_date['second_date'] = $year.'-'.$month.'-'.$second_day;
		}

		return $result_date;
	}

	private function getParsed($data_to_parse) {
		$data_parsed = [];

		$date_array = [];
		$data_parsed_last = 0;

		for ($i = 0; $i < count($data_to_parse); $i++) {
			if (preg_match('#[0-9]+[\s—-]+[0-9]+[\s]+[\w]+#iu', $data_to_parse->eq($i)->text())) {
				$date_array = $this->getDateFormatted(preg_split('#[\s,—-]+#iu', htmlspecialchars($data_to_parse->eq($i)->text())));
			}
			else if (preg_match('#[0-9]{2}:[0-9]{2}#', $data_to_parse->eq($i)->text())) {
				$data_parsed[] = [];
				$data_parsed_last = count($data_parsed)-1;
				$data_parsed[$data_parsed_last]['date'] = $date_array['date'];
				$data_parsed[$data_parsed_last]['second_date'] = $date_array['second_date'] != NULL ? $date_array['second_date'] : NULL;

				$data_parsed[$data_parsed_last]['time'] = htmlspecialchars($data_to_parse->eq($i)->text());
			}
			else if (preg_match('#[\w\s]*[2-3][D]#iu', $data_to_parse->eq($i)->text())) {
				$data_parsed[$data_parsed_last]['title'] = htmlspecialchars($data_to_parse->eq($i)->text());
			}
			else if (preg_match('#[0-9]{1,2}[+]#', $data_to_parse->eq($i)->text())) {
				$tmp = htmlspecialchars($data_to_parse->eq($i)->text());
				$data_parsed[$data_parsed_last]['rating'] = substr($tmp, 0, count($tmp)-2);
			}
			else if (preg_match('#[0-9]{1,3}[^+D]#', $data_to_parse->eq($i)->text())) {
				$data_parsed[$data_parsed_last]['price'] = (int)htmlspecialchars($data_to_parse->eq($i)->text());
			}
		}

		return $data_parsed;
	}

	private function getShowtimes() {
		$client = new Client();

		$crawler = $client->request('GET', 'http://cityopen.ru/?page_id=8831');

		$data_to_parse = $crawler->filter('td');
		$result_showtimes = $this->getParsed($data_to_parse);

		return $result_showtimes;
	}


	public function updateShowtimes() {
		$showtimes = $this->getShowtimes();

		DB::table('showtimes')->truncate();

		for ($i = 0; $i < count($showtimes); $i++) {
			DB::table('showtimes')->insert(
				['date' => $showtimes[$i]['date'], 'second_date' => $showtimes[$i]['second_date'], 'time' => $showtimes[$i]['time'], 'title' => $showtimes[$i]['title'], 'price' => $showtimes[$i]['price'], 'rating' => $showtimes[$i]['rating']]
				);
		}
	}

	public function getDates() {
		$dates = DB::table('showtimes')->lists('date', 'id');
		$second_dates = DB::table('showtimes')->lists('second_date', 'id');

		$tmp_dates = [];

		for($i = 1; $i <= count($dates); $i++) {
			if (!in_array([$dates[$i], $second_dates[$i]], $tmp_dates)) {
				$tmp_dates[] = [$dates[$i], $second_dates[$i]];
			}
		}

		$result_dates = [];

		for($i = 0; $i < count($tmp_dates); $i++) {
			if (count($tmp_dates[$i]) === 1) {
				$result_dates[] = $tmp_dates[0];
			}
			else {
				$date = preg_split('#-#', $tmp_dates[$i][0]);
				$fsrt_day = $date[2];
				$scnd_day = preg_split('#-#', $tmp_dates[$i][1])[2];
				for ($i = $fsrt_day; $i <= $scnd_day; $i++) {
					$result_dates[] = $date[0].'-'.$date[1].'-'.$i;
				}
			}
		}
		
		return $result_dates;
	}

	public function getSortedTable($get) {
		$table = DB::table('showtimes')
			->where('date', '<=', $get['date'])
			->where(function ($query) use ($get) {
			$query->whereNull('second_date')
				->orWhere('second_date', '>=', $get['date']);
		})
			->select('time', 'title', 'price', 'rating')
			->orderBy($get['subj'], $get['dir'])
			->get();

		return $table;
	}

	public $timestamps = false;
}