<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Showtime;

class ShowtimeController extends Controller
{
    public function index(Showtime $showtime) {
    	return view('showtime.index', ['panel' => 'panel-primary', 'size' => 10, 'offset' => 1]);
    }

    public function update(Showtime $showtime) {
    	$showtime->updateShowtimes();

    	return redirect()->back()->with('updated', 'Расписание было успешно обновлено');
    }

    public function get(Showtime $showtime) {
        $dates = $showtime->getDates();
        $subj_select = ['time' => 'времени', 'title' => 'названию', 'price' => 'цене', 'rating' => 'рейтингу'];
        $dir_select = ['asc' => 'возрастанию', 'desc' => 'убыванию'];
        $table = NULL;
        
        if (isset($_GET['date']) and isset($_GET['subj']) and isset($_GET['dir'])) {
            $table = $showtime->getSortedTable($_GET);
        }

    	return view('showtime.get', ['dates' => $dates, 'table' => $table, 'subj_select' => $subj_select, 'dir_select' => $dir_select, 'size' => 4, 'offset' => 4]);
    }
}
