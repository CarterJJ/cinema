<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|

*/

Route::get('/', ['as' => 'mainPage', 'uses' => "ShowtimeController@index"]);
Route::get('update', ['as' => 'update', 'uses' => "ShowtimeController@update"]);
Route::get('get', ['as' => 'getForm', 'uses' => "ShowtimeController@get"]);
